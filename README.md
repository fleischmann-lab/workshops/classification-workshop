# Workshop instructions

## Algorithms

Suggested classification algorithms to choose from (let me know in case your favorite algorithm is not in the list):
1) [Logistic regression](https://scikit-learn.org/stable/modules/generated/sklearn.linear_model.LogisticRegression.html) [Victor]
2) [K-Nearest Neighbors](https://scikit-learn.org/stable/modules/generated/sklearn.neighbors.NearestNeighbors.html) [Zach]
3) [Linear Discriminant Analysis](https://scikit-learn.org/stable/modules/generated/sklearn.discriminant_analysis.LinearDiscriminantAnalysis.html)
4) [Quadratic Discriminant Analysis](https://scikit-learn.org/stable/modules/generated/sklearn.discriminant_analysis.QuadraticDiscriminantAnalysis.html)
5) [Random Forest](https://scikit-learn.org/stable/modules/generated/sklearn.ensemble.RandomForestClassifier.html) [Andrea]
6) [Support-vector machine](https://scikit-learn.org/stable/modules/generated/sklearn.svm.LinearSVC.html) [Max]
7) Neural network ?

## Datasets
Suggested toy datasets to apply the classification task (if some people want to work with R or some other language, please let me know and I'm pretty sure we can adapt the datasets to work on the same ones from whatever programming language you prefer):
- [IRIS](https://scikit-learn.org/stable/modules/generated/sklearn.datasets.load_iris.html#sklearn.datasets.load_iris)
- [Breast cancer](https://scikit-learn.org/stable/modules/generated/sklearn.datasets.load_breast_cancer.html#sklearn.datasets.load_breast_cancer)
- *Optionnal: [Digits](https://scikit-learn.org/stable/modules/generated/sklearn.datasets.load_digits.html#sklearn.datasets.load_digits)*

## Expected outputs
- How did you choose to split the data?
- *Optional: [Resampling methods](https://scikit-learn.org/stable/modules/cross_validation.html) used: [leave-one-out cross-validation](https://scikit-learn.org/stable/modules/generated/sklearn.model_selection.LeaveOneOut.html), [k-fold cross-validation](https://scikit-learn.org/stable/modules/generated/sklearn.model_selection.KFold.html), bootstrap, etc.*
- What hyperparameters did you choose and why? (e.g. why did you choose k=2 or k=5 in the K-Nearest Neighbors case, and what does it changes? Or what threshold did you choose in the logistic regression case?)
- [Accuracy](https://scikit-learn.org/stable/modules/generated/sklearn.metrics.accuracy_score.html) results for each dataset
- [Confusion matrix](https://scikit-learn.org/stable/modules/model_evaluation.html#confusion-matrix)
- [AUC and ROC curve](https://www.scikit-yb.org/en/latest/api/classifier/rocauc.html)
- *Optional: Decision boundaries plot (if there are more than 3 dimensions, we would need to apply some dimensionality reduction technique first)*


Then the idea would be to concatenate all these results in this [shared presentation](https://docs.google.com/presentation/d/1bAUzZGIrWTBl_EbmeMWId7moEe9Xed77BTBp_x3IZjg/edit?usp=sharing).


# Software setup

## Prerequisites 🚧

1. The first prerequisite is to install the Conda package manager. There are two options to do that:
* [The Anaconda distribution](https://www.anaconda.com/products/individual), 
which is a huge full featured distribution with many things included, like a graphical interface.
* [The Miniconda distribution](https://docs.conda.io/en/latest/miniconda.html), which is a leaner distribution, and is command line only.

Miniconda is good if you know your way around the command line. Otherwise I would suggest to choose the Anaconda distribution.

2. The second prerequisite is to have Git installed. In case you don't have it already, you can download it from [here](https://git-scm.com/).

## Python environment 🐍

Clone this repository with Git:
```
git clone https://gitlab.com/fleischmann-lab/workshops/classification-workshop.git
```
This will create a new directory called `classification-workshop`. Go inside this directory with `cd classification-workshop`.

To create your environment from scratch, open a terminal
```
conda env create -f environment.yml
```
Then activate your environment:
```
conda activate classification-workshop
```

Add your new environment (kernel) in Jupyter:
```
python -m ipykernel install --user --name=classification-workshop
```

To make the extensions work in JupyterLab:
```
jupyter labextension install @jupyter-widgets/jupyterlab-manager jupyter-matplotlib @jupyterlab/debugger
```

## Time to start working finally 👷

If you made it this far, now you can run JupyterLab 🚀 with the following command:
```
jupyter lab
```

Once in JupyterLab, don't forget to change your kernel to classification-workshop.

If you later need to add new packages to your environment, just list them in the `environment.yml` file, and then update your environment with:
```
conda env update -f environment.yml
```
