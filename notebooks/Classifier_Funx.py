import numpy as np
from sklearn.metrics import accuracy_score, confusion_matrix
from sklearn.model_selection import RepeatedStratifiedKFold
from sklearn.preprocessing import MinMaxScaler
from sklearn.svm import SVC

scaler = MinMaxScaler()


def getRespsRegular(array, numTrialsToKeep, blinewin, odorwin):
    # baseline: 5.5-9 seconds, odorwindow: 10-13.3 seconds. numTrialsToKeep = 6 removed

    # array: trials, cells, odors, frames

    # average F every this frames after odor onset
    # numTrialsToKeep: optional

    ntrials, ncells, nodors, nframes = array.shape
    if not numTrialsToKeep:
        numTrialsToKeep = ntrials

    # set preodor baseline to zero and account for some nan frames by offsett
    resps = array[:numTrialsToKeep][..., odorwin[0] : odorwin[1]].mean(-1) - array[
        :numTrialsToKeep
    ][..., blinewin[0] : blinewin[1]].mean(-1)
    return resps


def getRespsRegularnobsline(array, numTrialsToKeep, odorwin):
    # baseline: 5.5-9 seconds, odorwindow: 10-13.3 seconds. numTrialsToKeep = 6 removed

    # array: trials, cells, odors, frames

    # average F every this frames after odor onset
    # numTrialsToKeep: optional

    ntrials, ncells, nodors, nframes = array.shape
    if not numTrialsToKeep:
        numTrialsToKeep = ntrials

    # set preodor baseline to zero and account for some nan frames by offsett
    resps = array[:numTrialsToKeep][..., odorwin[0] : odorwin[1]].mean(-1)
    return resps


def MeanResp(array, numTrialsToKeep=8, win=[45, 60]):

    # array: trials, cells, odors, frames

    ntrials, ncells, nodors, nframes = array.shape
    if not numTrialsToKeep:
        numTrialsToKeep = ntrials

    # set preodor baseline to zero and account for some nan frames by offsett
    resps = array[:numTrialsToKeep][..., win[0] : win[1]].mean(-1)

    return resps


def ConfusionMatrix(X, y, n_splits, n_repeats, kernel):
    y_predd = []
    y_testt = []
    svm_class = SVC(kernel=kernel)
    rskf = RepeatedStratifiedKFold(n_splits=n_splits, n_repeats=n_repeats)
    for train_index, test_index in rskf.split(X, y):
        X_train = X[train_index]
        X_test = X[test_index]
        y_train, y_test = y[train_index], y[test_index]
        X_train = scaler.fit_transform(X_train)
        X_test = scaler.transform(X_test)
        svm_class.fit(X_train, y_train)
        y_pred = svm_class.predict(X_test)

        y_predd.append(y_pred)
        y_testt.append(y_test)
    y_testt = np.concatenate(y_testt, 0)
    y_predd = np.concatenate(y_predd, 0)
    confusion = confusion_matrix(y_testt, y_predd)
    normalize1 = confusion / (n_splits * n_repeats)

    return normalize1


def SVModor(X, y, n_splits, n_repeats, kernel):
    scores = []
    meanscore = []
    svm_class = SVC(kernel=kernel)
    rskf = RepeatedStratifiedKFold(n_splits=n_splits, n_repeats=n_repeats)
    for train_index, test_index in rskf.split(X, y):
        X_train = X[train_index]
        X_test = X[test_index]
        y_train, y_test = y[train_index], y[test_index]
        X_train = scaler.fit_transform(X_train)
        X_test = scaler.transform(X_test)
        svm_class.fit(X_train, y_train)
        y_pred = svm_class.predict(X_test)
        scores.append(accuracy_score(y_test, y_pred)[None])
    meanscore = np.mean(scores)

    return scores, meanscore


# @jit
def SVMslidewindow(X, Xz, y, Numslidewin, n_splits, n_repeats, kernel):
    swinscore = []
    meanscore = []
    swinnomeanscore = []

    for i in range(Numslidewin):
        scores = []
        svm_class = SVC(kernel=kernel)
        rskf = RepeatedStratifiedKFold(n_splits=n_splits, n_repeats=n_repeats)
        for train_index, test_index in rskf.split(X, y):
            X_train = X[train_index]
            X_test = Xz[:, :, i][test_index]
            y_train, y_test = y[train_index], y[test_index]
            X_train = scaler.fit_transform(X_train)
            X_test = scaler.transform(X_test)
            svm_class.fit(X_train, y_train)
            y_pred = svm_class.predict(X_test)
            scores.append(accuracy_score(y_test, y_pred)[None])
        meanscore = np.mean(scores)
        swinscore.append(meanscore[None])
        swinnomeanscore.append(scores)
    swinscore = np.concatenate(swinscore, 0)
    swinnomeanscore = np.concatenate(swinnomeanscore, 1)

    return swinscore, swinnomeanscore


# @jit(nopython=False)
def SVMrandselectcells(X, y, numcells, n_splits, n_repeats):
    swinscore = []
    meanscore = []

    for i in range(1, numcells):
        index = np.random.choice(X.shape[1], i, replace=False)
        X_rand = X[:, index]
        scores = []
        svm_class = SVC(kernel="linear")
        rskf = RepeatedStratifiedKFold(n_splits=n_splits, n_repeats=n_repeats)
        for train_index, test_index in rskf.split(X_rand, y):
            X_train = X_rand[train_index]
            X_test = X_rand[test_index]
            y_train, y_test = y[train_index], y[test_index]
            X_train = scaler.fit_transform(X_train)
            X_test = scaler.transform(X_test)
            svm_class.fit(X_train, y_train)
            y_pred = svm_class.predict(X_test)
            scores.append(accuracy_score(y_test, y_pred)[None])
        meanscore = np.mean(scores)
        swinscore.append(meanscore[None])
    swinscore = np.concatenate(swinscore, 0)
    return swinscore
